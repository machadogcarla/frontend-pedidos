import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.pedidos',
  appName: 'pedidos',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
