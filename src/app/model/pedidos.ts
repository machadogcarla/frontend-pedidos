export interface Pedidos {
  id: string;
  status: string;
  valor: string;
  forma_pagamento: string;
  data_entrega: string;
  data_pedido: string;
}
