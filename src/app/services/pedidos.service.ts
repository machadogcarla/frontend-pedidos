import { Pedidos } from './../model/pedidos';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError} from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  apiUrl = '../../assets/pedidos.json';

  constructor(private httpClient: HttpClient) { }

  // Obtem todos os pedidos
  getPedidos(): Observable<Pedidos[]> {
    return this.httpClient.get<Pedidos[]>(this.apiUrl)
      .pipe(retry(1),
        catchError(this.handleError))
  }

  // Obtem um pedido pelo id
  getPedidoById(id: string): Observable<Pedidos> {
    console.log(id)
  return this.httpClient.get<Pedidos>(this.apiUrl)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };

}
