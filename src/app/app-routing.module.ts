import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaPedidosComponent } from './pages/lista-pedidos/lista-pedidos.component';
import { LoginComponent } from './pages/login/login.component';
import { PedidoAprovadoComponent } from './pages/pedido-aprovado/pedido-aprovado.component';
import { PedidoEntregueComponent } from './pages/pedido-entregue/pedido-entregue.component';
import { PedidoRecebidoComponent } from './pages/pedido-recebido/pedido-recebido.component';
import { PedidoSeparadoComponent } from './pages/pedido-separado/pedido-separado.component';
import { PedidoTransporteComponent } from './pages/pedido-transporte/pedido-transporte.component';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'login', component: LoginComponent },
  { path: 'pedido_recebido', component: PedidoRecebidoComponent},
  { path: 'pedido_aprovado', component: PedidoAprovadoComponent},
  { path: 'pedido_separado', component: PedidoSeparadoComponent},
  { path: 'pedido_transporte', component: PedidoTransporteComponent},
  { path: 'pedido_entregue', component: PedidoEntregueComponent},
  { path: 'lista_pedidos', component: ListaPedidosComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

