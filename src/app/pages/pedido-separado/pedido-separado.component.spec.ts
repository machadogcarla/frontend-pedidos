import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoSeparadoComponent } from './pedido-separado.component';

describe('PedidoSeparadoComponent', () => {
  let component: PedidoSeparadoComponent;
  let fixture: ComponentFixture<PedidoSeparadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PedidoSeparadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoSeparadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
