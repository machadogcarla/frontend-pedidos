import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalLoginComponent } from 'src/app/components/modal-login/modal-login.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string = '';
  loginBD: string = 'admin';
  passwordBD: string = '1234';
  error = 'Login inválido, tente novamente.';

  constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private modal: NgbModal
  ) {
    this.router = router;
  }

  ngOnInit() {
      this.loginForm = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', Validators.required]
      });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
      this.submitted = true;
      if (this.loginForm.invalid) {
          return;
      }

      if((this.f.username.value == this.loginBD) && (this.f.password.value == this.passwordBD) ){
        this.loading = true;
        this.router.navigate(['/lista_pedidos']);
      }
      else{
        this.openDialog('');
      }
    }

    openDialog(dados : any){
      const modalRef = this.modal.open(ModalLoginComponent, { size: 'xl' });
      modalRef.componentInstance.error = this.error;
    }
}
