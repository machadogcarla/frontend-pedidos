import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/components/modal-itens-pedido/modal.component';
import { NotafiscalComponent } from 'src/app/components/modal-notafiscal/notafiscal.component';

@Component({
  selector: 'app-dados-pedido',
  templateUrl: './dados-pedido.component.html',
  styleUrls: ['./dados-pedido.component.css']
})
export class DadosPedidoComponent implements OnInit {

  data_entrega = '6 de agosto de 2021';
  valor_total = '1280,56';
  forma_pagamento = 'Boleto - 14 dias';
  total_descontos = '58,15';
  end_entrega = 'QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE';
  itens_pedido = [
    {nome: "Shampoo", qtd: 100},
    {nome: "Condicionador", qtd: 110},
    {nome: "Creme de pentear", qtd: 120},
    {nome: "Shampoo", qtd: 200},
    {nome: "Condicionador", qtd: 130},
    {nome: "Creme de pentear", qtd: 120},
    {nome: "Shampoo", qtd: 120},
    {nome: "Condicionador", qtd: 150},
    {nome: "Creme de pentear", qtd: 130}
  ]

  constructor(private modal: NgbModal) { }

  ngOnInit(): void {
  }

  openModalItens(dados : any) {
    const modalRef = this.modal.open(ModalComponent, { size: 'xl' });
    modalRef.componentInstance.itens_pedido = this.itens_pedido
  }

  openModalNotaFiscal(dados : any) {
    const modalRef = this.modal.open(NotafiscalComponent, { size: 'xl' });
    modalRef.componentInstance.itens_pedido = this.itens_pedido
  }
}
