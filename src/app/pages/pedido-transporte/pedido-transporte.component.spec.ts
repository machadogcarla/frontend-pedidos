import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoTransporteComponent } from './pedido-transporte.component';

describe('PedidoTransporteComponent', () => {
  let component: PedidoTransporteComponent;
  let fixture: ComponentFixture<PedidoTransporteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PedidoTransporteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoTransporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
