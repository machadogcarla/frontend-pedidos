import { Pedidos } from './../../model/pedidos';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable, of } from 'rxjs';
import { PedidosService } from 'src/app/services/pedidos.service';

@Component({
  selector: 'app-lista-pedidos',
  templateUrl: './lista-pedidos.component.html',
  styleUrls: ['./lista-pedidos.component.css']
})
export class ListaPedidosComponent implements OnInit {

  idBuscado: any;
  peds: Pedidos[];
  ped: any;
  constructor(private pedidosService: PedidosService) {}

  ngOnInit() {
    this.getPedidos();
  }

    // Chama o serviço para obtém todos os pedidos
    getPedidos() {
      this.pedidosService.getPedidos().subscribe((peds: Pedidos[]) => {
        this.peds = peds;
      });
    }

    getPedidoById(){
      this.pedidosService.getPedidoById(this.idBuscado).subscribe(result => this.ped = result);
    }

}
