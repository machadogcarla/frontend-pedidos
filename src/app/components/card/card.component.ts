import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() lista_pedidos : any;

  constructor(private router: Router) {this.router = router; }

  ngOnInit(): void {
  }

  status_pedido(pedido_status : any){
    if(pedido_status == 'Recebido'){
      this.router.navigate(['/pedido_recebido']);
    }
    if(pedido_status == 'Aprovado'){
      this.router.navigate(['/pedido_aprovado']);
    }
    if(pedido_status == 'Separado'){
      this.router.navigate(['/pedido_separado']);
    }
    if(pedido_status == 'Transporte'){
      this.router.navigate(['/pedido_transporte']);
    }
    if(pedido_status == 'Entregue'){
      this.router.navigate(['/pedido_entregue']);
    }
  }

}
