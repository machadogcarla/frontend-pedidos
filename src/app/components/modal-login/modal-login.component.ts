import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.css']
})
export class ModalLoginComponent implements OnInit {

  @Input() error : [];

  constructor() { }

  ngOnInit(): void {
  }

}
