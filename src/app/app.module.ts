import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { DadosPedidoComponent } from './pages/dados-pedido/dados-pedido.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PedidoRecebidoComponent } from './pages/pedido-recebido/pedido-recebido.component';
import { PedidoAprovadoComponent } from './pages/pedido-aprovado/pedido-aprovado.component';
import { PedidoSeparadoComponent } from './pages/pedido-separado/pedido-separado.component';
import { PedidoTransporteComponent } from './pages/pedido-transporte/pedido-transporte.component';
import { PedidoEntregueComponent } from './pages/pedido-entregue/pedido-entregue.component';
import { MenuComponent } from './components/menu/menu.component';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './components/modal-itens-pedido/modal.component';
import { NotafiscalComponent } from './components/modal-notafiscal/notafiscal.component';
import { ListaPedidosComponent } from './pages/lista-pedidos/lista-pedidos.component';
import { CardComponent } from './components/card/card.component';
import { FormsModule } from '@angular/forms';
import { ModalLoginComponent } from './components/modal-login/modal-login.component'
import { HttpClientModule  } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DadosPedidoComponent,
    PedidoRecebidoComponent,
    PedidoAprovadoComponent,
    PedidoSeparadoComponent,
    PedidoTransporteComponent,
    PedidoEntregueComponent,
    MenuComponent,
    ModalComponent,
    NotafiscalComponent,
    ListaPedidosComponent,
    CardComponent,
    ModalLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AppRoutingModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
